#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

void CopyMatrixFloat(float* matrixNew, float* matrixOld, int totalRows, int totalCols);

int MatrixValueInt(int* array, int row, int col,int totalCols);

void Matrix2TextChar(char *fileName,char *array, int totalRows, int totalCols);
void Matrix2TextFloat(char *fileName,float *array, int totalRows, int totalCols);
void Matrix2TextInt(char *fileName,int *array, int totalRows, int totalCols);

int Text2MatrixDouble(char *fileName, double *array, int totalCols);
int Text2MatrixFloat(char *fileName, float *array, int totalCols);
int Text2MatrixInt(char *fileName, int *array, int totalCols);

void PrintMatrixFloat(float *array,int totalRows, int totalCols);
void PrintMatrixInt(int *array,int totalRows, int totalCols);

void SortInt(int array[],int len); 

void NonzeroToFrontInt(int array[],int len); 

double RandDouble(double min, double max);
int RandInt(int min, int max);

void BinArrayInt(int array[],int lenArray, int bins[], int min, int max,int lenBins);

bool ChiSquared(int observed[],double expected[],int len);

void InputFromTerminalString(char* stringInput,char **argv); 

int ArraySumInt(int array[],int len);// Need test

void Matrix2TextLong(char *fileName,long* array, long totalRows, long totalCols);//need test

float nonzeroAverageInt(int array[],int len);

int sumOfArrayInt(int array[],int len);