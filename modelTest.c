#include <stdio.h>
#include <math.h>
#include "lauren.h"
#include "minunit.h"
#include "modelLib.h"

int tests_run = 0;

static char * antibioticDegradation_test(){
  float antibioticConc[TOTALROWS][TOTALCOLS] = {0}; //2D antibiotic concentration matrix
  
  float dt = 0.1; //temporal resolution step
  float k_degrade = -20;

  char cellType[TOTALROWS][TOTALCOLS];

  int ii,jj;
  for (ii=2;ii<6;ii++){ //Initializes matrix with antibiotic
    for(jj=2;jj<6;jj++){
      antibioticConc[ii][jj] = 100;
    }
  }

  for (ii=4;ii<8;ii++){ //Initializes matrix with antibiotic
    for(jj=4;jj<8;jj++){
      cellType[ii][jj] = 'r';
    }
  }


  //antibioticDegradation((char *)cellType, (float *)antibioticConc,k_degrade,TOTALROWS,TOTALCOLS,dt); //Each with certain 'r' cell type degrades antibiotic
  
  return 0;
}

static char * diffusion2D_test() {
  float antibioticConc[TOTALROWS][TOTALCOLS] = {0}; //2D antibiotic concentration matrix
  float dx = 0.01, dy = 0.01; //spacial resolution step
  float dt = pow(dx,2)/4; //temporal resolution step

  int ii,jj;
  for (ii=4;ii<8;ii++){ //Initializes matrix with antibiotic
    for(jj=4;jj<8;jj++){
      antibioticConc[ii][jj] = 100;
    }
  }
  diffusion2D((float *)antibioticConc,TOTALROWS, TOTALCOLS,dt,dx,dy); //One 2D diffusion step
  float matlabSolution[TOTALROWS][TOTALCOLS];
  Text2MatrixFloat("smallDiffusion.txt", (float *) matlabSolution, TOTALCOLS);

  float diff = 0;
  for (ii=0;ii<TOTALROWS;ii++){ //Initializes matrix with antibiotic
    for(jj=0;jj<TOTALCOLS;jj++){
      diff += abs(antibioticConc[ii][jj] - matlabSolution[ii][jj]);
    }
  }
  printf("%f\n",diff);
  mu_assert("Difference in antibiotic conc = %f", (int)diff ==0);
  return 0;
}

static char * all_tests() {
  mu_run_test(diffusion2D_test);
  antibioticDegradation_test();
  return 0;
}

int main(){
  char *result = all_tests();
  if (result != 0) {
    printf("%s\n", result);
  }
  else {
    printf("ALL TESTS PASSED\n");
  }
  printf("Tests run: %d\n", tests_run);

  return result != 0;
}