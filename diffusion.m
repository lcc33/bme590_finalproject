clear; close all;

dx = 0.01;
xLen = 10;
dy = dx;
   
T = zeros(xLen);
T(5:8,5:8) = 100;
dt = dx^2/4;
t_current = 0;
total_time = 0.1;

%while (t_current<total_time)
  t_current = t_current+dt;
  Told = T;
 % imagesc(Told);
  for i = 2:xLen-1
      for j = 2:xLen-1
        T(i,j) = dt*((Told(i+1,j)-2*Told(i,j)+Told(i-1,j))/dx^2 ... 
              + (Told(i,j+1)-2*Told(i,j)+Told(i,j-1))/dy^2) ...
              + Told(i,j);
      end
  end
%end

fileID = fopen('matlabOutput.txt', 'w');
for ii=1:xLen
  for jj=1:xLen 
    fprintf(fileID,'%f ', T(ii,jj));
  end
  fprintf(fileID,'\n');
end
fclose(fileID);

