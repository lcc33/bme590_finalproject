#include "modelLib.h"

#define TOTALROWS 101
#define TOTALCOLS 101

int main(){
  float antibioticConc[TOTALROWS][TOTALCOLS] = {0}; //2D antibiotic concentration matrix
  
  float dx = 0.01, dy = 0.01; //spacial resolution step

  float dt = pow(dx,2)/4; //temporal resolution step
  float tCurrent = 0; //initialize time at zero
  float totalTime = 0.1; //total time to be modeled
  float k_degrade = -0.5;

  char cellType[TOTALROWS][TOTALCOLS];

  int ii,jj;
  for (ii=10;ii<20;ii++){ //Initializes matrix with antibiotic
    for(jj=10;jj<20;jj++){
      antibioticConc[ii][jj] = 10000;
    }
  }

  for (ii=80;ii<90;ii++){ //Initializes matrix with antibiotic
    for(jj=80;jj<90;jj++){
      cellType[ii][jj] = 'r';
    }
  }

  while (tCurrent<totalTime){ //increments through time
    tCurrent += dt; //add t-step to current time
    
    diffusion2D((float *)antibioticConc, TOTALROWS, TOTALCOLS,dt,dx,dy); //One 2D diffusion step
    antibioticDegradation((char *)cellType, (float *)antibioticConc,k_degrade,TOTALROWS,TOTALCOLS,dt); //Each with certain 'r' cell type degrades antibiotic
  }

  Matrix2TextFloat("cOutput.txt", (float *)antibioticConc,TOTALROWS,TOTALCOLS); //Prints out final matrix
  //PrintMatrixFloat((float *)antibioticConc,TOTALROWS,TOTALCOLS);
  return 0;
}