#include <stdio.h>
#include <math.h>

void diffusion2D(float *antibioticConc, int totalRows, int totalCols,float dt,float dx,float dy);

void antibioticDegradation(char *cellType, float *antibioticConc,float k_degrade,int totalRows,int totalCols,float dt);