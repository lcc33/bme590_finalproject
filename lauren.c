#include "lauren.h"

/////////////////////////INPUT OUTPUT/////////////////////////

void CopyMatrixFloat(float* matrixNew, float* matrixOld, int totalRows, int totalCols){
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for(jj=0;jj<totalCols;jj++){
      *((matrixOld+ii*totalCols) + jj) = *((matrixNew+ii*totalCols) + jj);
    }
  }
}

int MatrixValueInt(int* array, int row, int col,int totalCols){
  return *(array + row * totalCols + col);
}

void Matrix2TextChar(char *fileName,char *array, int totalRows, int totalCols){
  FILE *write; //Initiate output file
  write = fopen(fileName,"w");
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for (jj=0;jj<totalCols;jj++){
      fprintf(write,"%c ",*((array+ii*totalCols) + jj));
    }
    fprintf(write,"\n");
  }
  fclose(write);
}

void Matrix2TextFloat(char *fileName,float *array, int totalRows, int totalCols){
  FILE *write; //Initiate output file
  write = fopen(fileName,"w");
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for (jj=0;jj<totalCols;jj++){
      fprintf(write,"%f ",*((array+ii*totalCols) + jj));
    }
    fprintf(write,"\n");
  }
  fclose(write);
}

void Matrix2TextInt(char *fileName,int *array, int totalRows, int totalCols){
  FILE *write; //Initiate output file
  write = fopen(fileName,"w");
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for (jj=0;jj<totalCols;jj++){
      fprintf(write,"%d ",*((array+ii*totalCols) + jj));
    }
    fprintf(write,"\n");
  }
  fclose(write);
}

int Text2MatrixDouble(char *fileName, double *array, int totalCols){
  FILE *read;
  int rowNum=0, colNum=0;

  read = fopen(fileName,"r");
  if (read == NULL){
    printf("Could not open file: %s\n",fileName);
  }
  else {
    while (!feof(read)){
      fscanf(read,"%lf",((array+rowNum*totalCols) + colNum));
      if (colNum == totalCols-1){
        rowNum++;
        colNum = 0;
      }
      else {
        colNum++;
      }
    }
  }
  return rowNum;
  fclose(read);
}

int Text2MatrixFloat(char *fileName, float *array, int totalCols){
  FILE *read;
  int rowNum=0, colNum=0;

  read = fopen(fileName,"r");
  if (read == NULL){
    printf("Could not open file: %s\n",fileName);
  }
  else {
    while (!feof(read)){
      fscanf(read,"%f",((array+rowNum*totalCols) + colNum));
      if (colNum == totalCols-1){
        rowNum++;
        colNum = 0;
      }
      else {
        colNum++;
      }
    }
  }
  return rowNum;
  fclose(read);
}

int Text2MatrixInt(char *fileName, int *array, int totalCols){
  FILE *read;
  int rowNum=0, colNum=0;

  read = fopen(fileName,"r");
  if (read == NULL){
    printf("Could not open file: %s\n",fileName);
  }
  else {
    while (!feof(read)){
      fscanf(read,"%d",((array+rowNum*totalCols) + colNum));
      if (colNum == totalCols-1){
        rowNum++;
        colNum = 0;
      }
      else {
        colNum++;
      }
    }
  }
  return rowNum;
  fclose(read);
}

void PrintMatrixFloat(float *array,int totalRows, int totalCols){
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for (jj=0;jj<totalCols;jj++){
      printf("%f ",*((array+ii*totalCols) + jj));
    }
    printf("\n");
  }
}


void PrintMatrixInt(int *array,int totalRows, int totalCols){
  int ii,jj;
  for (ii=0;ii<totalRows;ii++){
    for (jj=0;jj<totalCols;jj++){
      printf("%d ",*((array+ii*totalCols) + jj));
    }
    printf("\n");
  }
}


void SortInt(int array[],int len){
  int ii,jj;
  int lowerNum;

  for (ii=0;ii<len-1;ii++){
    for (jj=0;jj<len-ii-1;jj++){
      if (array[jj]>array[jj+1]){
        lowerNum = array[jj+1];
        array[jj+1] = array[jj];
        array[jj] = lowerNum;
      }
    }
  }
}

void NonzeroToFrontInt(int array[],int len){
  int ii,jj;

  for (ii=0;ii<len-1;ii++){
    for (jj=0;jj<len-ii-1;jj++){
      if (array[jj]==0 && array[jj+1] != 0){
        array[jj] = array[jj+1];
        array[jj+1] = 0;
      }
    }
  }
}

/////////////////////////RANDOM NUMBERS/////////////////////////

double RandDouble(double min, double max){
  return (double)rand()/(double)(RAND_MAX/(max-min))+min;
}

int RandInt(int min, int max){
  int size = max - min;
  return (random() % size)+min;
}


/////////////////////////STATISTICS/////////////////////////

void BinArrayInt(int array[],int lenArray, int bins[], int min, int max,int lenBins){
  int ii,jj;
  for (ii=0;ii<lenArray;ii++){//bin numbers;
    for (jj=0;jj<lenBins;jj++){
      if (array[ii]<((max-min)*(jj+1)/lenBins+min)){
        bins[jj]++;
        break;
      }
    }
  }
}


bool ChiSquared(int observed[],double expected[],int len){
  double chi=0;
  double obtained;
  int ii;
  int cols=2;
  int row=1;
  double chiSquaredTableValues[266][2];
  for (ii=0;ii<len;ii++){
    obtained = (double) observed[ii];
    chi += pow(((obtained-expected[ii])),2)/expected[ii];
  }

  Text2MatrixDouble("chi.txt", (double *)chiSquaredTableValues, cols);
  while (chiSquaredTableValues[row][0] < len-1){
    row++;
  }
  if (chiSquaredTableValues[row][1] > chi){
    return true;
  }
  else {
    return false;
  }
  
}

void InputFromTerminalString(char* stringInput,char **argv){
  sscanf(argv[1],"%s",stringInput); //Search for file name in terminal input
}

/////////////////////////NEED UNIT TESTS/////////////////////////
int ArraySumInt(int array[],int len){ 
  int ii;
  int output=0;
  for (ii=0;ii<len;ii++){
    output += array[ii];
  }
  return output;
}

void Matrix2TextLong(char *fileName,long* array, long totalRows, long totalCols){
  FILE *write; //Initiate output file
  write = fopen(fileName,"w");
  long ii;
  long rowNum=0;
  while (rowNum<totalRows) { //Write primes to output file one integer per line
    for(ii=0;ii<totalCols;ii++){
      fprintf(write,"%ld ", *(array + rowNum * totalCols + ii));
    }
    fprintf(write,"\n");
    rowNum++;
  }
  fclose(write);
}

float nonzeroAverageInt(int array[],int len){
   long loop;
   float avg;
   long sum=0;
   long totalNonzero=0;

   sum = avg = 0;
   
   for(loop = 0; loop < (long)len; loop++) {
     if(array[loop] !=0){
       sum = sum + (long)array[loop];
       totalNonzero++;
     }
   }
   avg = (float)sum / (float)totalNonzero;
   return avg;
}

int sumOfArrayInt(int array[],int len){
  int ii;
  int sum=0;
  for(ii=0;ii<len;ii++){
    sum+=array[ii];
  }
  return sum;
}