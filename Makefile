CC = gcc
CFLAGS = -Wall -D TOTALCOLS=10 -D TOTALROWS=10
LDFLAGS = -lm 
OBJFILES = modelTest.o modelLib.o lauren.o
TARGET = modelTest

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES) $(LDFLAGS)

clean:
	rm -f $(OBJFILES) $(TARGET) *~