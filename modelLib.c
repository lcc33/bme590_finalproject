#include "modelLib.h"

void diffusion2D(float *antibioticConc, int totalRows, int totalCols,float dt,float dx,float dy){
  float antibioticConcOld[TOTALROWS][TOTALCOLS] = {0}; //2D matrix to store antibiotic from one time step ago
  int ii,jj;
  CopyMatrixFloat((float *) antibioticConc,(float *) antibioticConcOld, TOTALROWS, TOTALCOLS); //copies one matrix to another to store
  for (ii=1;ii<totalRows-1;ii++){
    for (jj=1;jj<totalCols-1;jj++){
      *((antibioticConc+ii*totalCols) + jj) = dt*((antibioticConcOld[ii+1][jj]-2*antibioticConcOld[ii][jj]+antibioticConcOld[ii-1][jj])/pow(dx,2)+ (antibioticConcOld[ii][jj+1]-2*antibioticConcOld[ii][jj] +antibioticConcOld[ii][jj-1])/pow(dy,2)) + antibioticConcOld[ii][jj];
    }
  }
}

void antibioticDegradation(char *cellType, float *antibioticConc,float k_degrade,int totalRows,int totalCols,float dt){
  int ii, jj;
  for (ii=0;ii<totalRows;ii++){ //Initializes matrix with antibiotic
    for(jj=0;jj<totalCols;jj++){
      if (*((cellType+ii*totalCols) + jj) == 'r'){
        *((antibioticConc+ii*totalCols) + jj) += k_degrade*(*((antibioticConc+ii*totalCols) + jj))*dt;
      }
    }
  }
}